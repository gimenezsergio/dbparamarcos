package entidades;

public class Productos {
    private String name;
    private int cantidad;

    public Productos(String nombre, int cant) {
        this.name = nombre;
        this.cantidad = cant;
    }
    
    public void test() {
        System.out.println("Ejemplo de Giovannnny");
    }

    public int getCant() {
        return cantidad;
    }

    public void setCant(int cant) {
        this.cantidad = cant;
    }

    public String getNombre() {
        return name;
    }

    public void setNombre(String nombre) {
        this.name = nombre;
    }

    @Override
    public String toString() {
        return "Productos{" + "nombre=" + name + ", cant=" + cantidad + '}';
    }
    
}
